from django.apps import AppConfig


class NicheClonMakerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'niche_clon_maker'
