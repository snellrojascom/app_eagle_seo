# Simple SEO app to make some automatizations

This app has 3 main subapps that will be able to make
some automatizations for SEO tasks.

## The apps:

### GeoLoc

Simple app to write exif metadato on images that need
to be geolocalized, very useful for local SEO strategies.

### Images Maker

This app is able to create images in bulk from the 
main keywords of your SEO project.

The images generated can be used as media for social
networks as Pinterest or facebook.

### Niche Clon Maker

This app is a balck hat SEO technique that allow to 
you to create 'clones' of other SEO niche projects.

The app need to work beside Screaming Frog to obtain
the desired results and deliver to you an XML document
with all the pages/categories and blog posts tha was
be cloned.
